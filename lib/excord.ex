defmodule Excord do
  @moduledoc """
  Documentation for Excord.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Excord.hello
      :world

  """
  def hello do
    :world
  end
end
